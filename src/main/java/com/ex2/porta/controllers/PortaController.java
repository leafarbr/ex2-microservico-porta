package com.ex2.porta.controllers;

import com.ex2.porta.models.Porta;
import com.ex2.porta.services.PortaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/porta")
public class PortaController {

    @Autowired
    private PortaService portaService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    Porta cadastraPorta(@RequestBody Porta pPorta) {
        return portaService.cadastraPorta(pPorta);
    }

    @GetMapping("/{id}")
    Porta consultarPorId(@PathVariable(name = "id") int idPorta) {

        return portaService.consultarPortaPorId(idPorta);

    }
    
    
}
