package com.ex2.porta.repositories;

import com.ex2.porta.models.Porta;
import net.bytebuddy.implementation.bytecode.constant.IntegerConstant;
import org.springframework.data.repository.CrudRepository;

public interface PortaRepository extends CrudRepository<Porta, Integer> {

}
