package com.ex2.porta.services;

import com.ex2.porta.exceptions.PortaNaoEncontradaException;
import com.ex2.porta.models.Porta;
import com.ex2.porta.repositories.PortaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PortaService {

    @Autowired
    private PortaRepository portaRepository;


    public Porta cadastraPorta(Porta pPorta) {
        return portaRepository.save(pPorta);
    }

    public Porta consultarPortaPorId(int id) {
        Optional<Porta> objPorta = portaRepository.findById(id);

        if (objPorta.isPresent()) {
            return objPorta.get();
        } else {
            throw new PortaNaoEncontradaException();
        }
    }

}
